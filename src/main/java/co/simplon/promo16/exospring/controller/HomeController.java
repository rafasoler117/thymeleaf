package co.simplon.promo16.exospring.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {
    
    private String message ="test";

    @GetMapping("/")
    public String ShowHomePage(Model model){
        model.addAttribute("message", message);
        Random rand = new Random();
        model.addAttribute("chiffre",rand.nextInt((10-0)+1)+0);
        return "index";
    }

    @GetMapping("/loop")
    public String looping(Model model){
        List<String> listString = new ArrayList<>(List.of("yo","test","tavu"));
    
        model.addAttribute("list", listString);

        return "loop";
    }
    @GetMapping("/form")
    public String showForm(Model model){
        System.out.println("From Get");
        return "form";
    }
    @PostMapping("/form")
    public String processForm(@ModelAttribute("message") String message){
        System.out.println("From Post");
        System.out.println(message);
        return "form";
    }
}
