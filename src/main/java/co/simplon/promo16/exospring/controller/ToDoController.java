package co.simplon.promo16.exospring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.promo16.exospring.entity.PostIt;

@Controller
public class ToDoController {
    
    private List<PostIt> list = new ArrayList<>(List.of
    (new PostIt("title1", "content1"),
     new PostIt("title2", "content2"),
    new PostIt("title3", "content3")));
    
    
       
    
    @GetMapping("/todo")
    public String toDoo(Model model){
        model.addAttribute("list", list);
        model.addAttribute("postIt", new PostIt());
        return "todo";
    }
    @PostMapping("/todo")
    public String toDooPost(@ModelAttribute PostIt postIt){
        list.add(postIt);
       
        return "redirect:/todo";
    }
}
